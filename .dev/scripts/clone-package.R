################################################################################
## Update Robyn
################################################################################
# Events ------------------------------------------------------------------
events <- new.env()

events$line <- function() paste(rep("#", 40), collapse = "")
events$print <- function(msg) print(stringr::str_glue("{line}\n## {msg}\n{line}", line = events$line(), msg = msg))

events$DOWNLOADED_NEW_VERSION <- function() events$print("Download new version")
events$DELETED_OLD_VERSION    <- function() events$print("Deleted old version")
events$COPIED_NEW_VERSION     <- function() events$print("Copied new version")
events$FINISHED               <- function() events$print("Tidied up")


# Steps -------------------------------------------------------------------
download_new_version <- function(session, path = getwd()){
  session$root_path <- path
  session$robyn_zip_path <- tempfile("robyn-", fileext = ".zip")
  session$robyn_unzip_path <- tempfile("robyn-file_dump-")
  session$robyn_R_path <- tempfile("robyn-R-")


  download.file(url = "https://github.com/facebookexperimental/Robyn/archive/master.zip", destfile = session$robyn_zip_path)
  unzip(zipfile = session$robyn_zip_path, exdir = session$robyn_unzip_path)
  fs::dir_copy(file.path(session$robyn_unzip_path, "Robyn-main", "R"), session$robyn_R_path)
  file.rename(fs::path(session$robyn_R_path), fs::path(dirname(session$robyn_R_path), basename(session$root_path)))
  session$robyn_R_path <- fs::path(dirname(session$robyn_R_path), basename(session$root_path))

  events$DOWNLOADED_NEW_VERSION()
  return(session)
}

delete_old_version <- function(session){
  invisible(
    files_to_delete <- tibble::tibble(file = list.files(session$root_path, all.files = FALSE))
    |> dplyr::filter(!file %in% c("bitbucket-pipelines.yml", ".git", ".dev"))
    |> dplyr::pull()
  )

  unlink(files_to_delete, recursive = TRUE)
  events$DELETED_OLD_VERSION()
  return(session)
}

copy_new_version <- function(session){
  files_source <- list.files(session$robyn_R_path, recursive = TRUE, all.files = TRUE, full.names = TRUE)
  files_target <- stringr::str_replace_all(files_source, session$robyn_R_path, session$root_path)

  for(path in unique(dirname(files_target))) suppressWarnings(dir.create(path, recursive = TRUE))
  file.copy(files_source, files_target, overwrite = TRUE)

  events$COPIED_NEW_VERSION()
  return(session)
}

tidy_up <- function(session){
  withr::local_dir(usethis::proj_get())
  devtools::document()
  usethis::use_build_ignore(c("bitbucket-pipelines.yml", ".dev/"))

  events$FINISHED()
  return(session)
}


# Workflow ----------------------------------------------------------------(
session <- new.env()
session <- session |> download_new_version()
session <- session |> delete_old_version()
session <- session |> copy_new_version()
session <- session |> tidy_up()


# Test Package ------------------------------------------------------------
devtools::check(error_on = 'note')

