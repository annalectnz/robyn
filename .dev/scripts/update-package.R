# setup -------------------------------------------------------------------
remotes::install_cran("git2r")


# Configuration -----------------------------------------------------------
address <- list(
  github = "https://github.com/facebookexperimental/Robyn.git",
  bitbucket = "https://bitbucket.org/annalectnz/robyn.git"
)


# Helpers -----------------------------------------------------------------
tag_and_push_to_remote <- function(tag_name) {
  system(stringr::str_glue("git tag {tag_name} -f", tag_name = tag_name))
  system(stringr::str_glue("git push origin {tag_name}", tag_name = tag_name))
}

get_tags <- function(address) { return(
  tibble::tibble(tag = system(stringr::str_glue("git ls-remote --tags {address}", address = address), intern = TRUE))
  |> dplyr::mutate(
    .tag = stringr::str_extract(tag, "/v.*[0-9]$"),
    .tag = stringr::str_remove(.tag, "/v")
  )
  |> tidyr::drop_na()
  |> dplyr::transmute(tag = .tag)
  |> dplyr::distinct()
) }

is_new_tag_available <- function(tags) { return(
  tags
  |> dplyr::group_by(source)
  |> dplyr::slice_max(order_by = tag, n = 1)
  |> tidyr::pivot_wider(names_from = source, values_from = tag)
  |> dplyr::mutate(new_version_flag = github > bitbucket)
  |> dplyr::pull(new_version_flag)
) }


# Update Package ----------------------------------------------------------
tags <- dplyr::bind_rows(github = get_tags(address$github), bitbucket = get_tags(address$bitbucket), .id = "source")

if(is_new_tag_available(tags)){
  # Clone changes
  source(usethis::proj_path(".dev", "scripts", "clone-package", ext = "R"), local = TRUE, echo = TRUE)

  # Commit changes
  new_tag <- tags |> dplyr::filter(source %in% "github") |> dplyr::pull(tag) |> max()

  git2r::commit(
    usethis::proj_get(),
    message = stringr::str_glue("Update to {tag}", tag = new_tag),
    all = TRUE
  )

  # Push changes
  cred <- git2r::cred_env(username = Sys.getenv("BITBUCKET_USER"), password = Sys.getenv("BITBUCKET_PASSWORD"))
  git2r::push(usethis::proj_get(), credentials = cred)

  # Tag version
  tag_and_push_to_remote(tag_name = stringr::str_glue("v{tag_name}", tag_name = new_tag))

  # Event
  print(stringr::str_glue("Updated Robyn to v{tag_name}", tag_name = new_tag))
} else {
  print("Skipped Robyn update")
}
